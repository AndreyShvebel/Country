# Country

### Run backend locally
```shell script
cd backend
```
#### Optionally use venv
```shell script
python3 -m venv env
source ./env/bin/activate
```
#### Install dependencies
```shell script
pip install -r requirements.txt
```
#### Run migrations (make sure you have db `hakaton` or change `sqlite` to `default` in `settings.py`)
```shell script
python manage.py migrate
```
#### Run dev server
```shell script
python manage.py runserver
```

### Run frontend locally
```shell script
cd frontend
```
#### Install dependencies
```shell script
npm install
```
#### Run dev server
```shell script
npm run start
```
#### Build
```shell script
npm run build
```
