from django.db import models
from django.db.models.aggregates import Max
from django.template.defaultfilters import slugify

class EnrolleeCategory(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "categories"
