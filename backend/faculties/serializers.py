from rest_framework import serializers
from .models import Faculty, Department, Unit, RequiredExamsList


class FacultySerializer(serializers.ModelSerializer):
    class Meta:
        model = Faculty
        fields = ('id', 'name', 'slug', 'dean', 'faculty_phone', 'faculty_office', 'faculty_email')


class DepartmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Department
        fields = ('id', 'name', 'department_phone', 'department_office', 'department_email', 'faculty')


class UnitSerializer(serializers.ModelSerializer):
    required_exam1 = serializers.StringRelatedField()
    required_exam2 = serializers.StringRelatedField()
    required_exam3 = serializers.StringRelatedField()
    
    class Meta:
        model = Unit
        fields = ('id', 'name', 'description', 'description', 'passing_score', 'required_exam1', 'required_exam2', 'required_exam3')


class RequiredExamsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = RequiredExamsList
        fields = ('id', 'name')


class FacultyDetailsSerializer(serializers.ModelSerializer):
    departments = DepartmentSerializer(many=True)
    units = UnitSerializer(many=True)

    class Meta: 
        model = Faculty
        fields = ('id', 'name', 'dean', 'faculty_phone', 'faculty_office', 'faculty_email', 'departments', 'units')




