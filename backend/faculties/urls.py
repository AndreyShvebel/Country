from django.urls import path

from .views import FacultiesView, DepartmentView, UnitView, FacultyDetailsView

urlpatterns = [
    path('', FacultiesView.as_view()),
    path('<slug>/', FacultyDetailsView.as_view()),
    path('<slug>/department', DepartmentView.as_view()),
    path('<slug>/unit', UnitView.as_view()),
]