from django.contrib import admin
from .models import Faculty, Department, Unit, RequiredExamsList


class UnitInline(admin.StackedInline):
    model = Unit


class DepartmentInline(admin.StackedInline):
    model = Department


class RequiredExamsListModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_display_links = ('id', 'name')


class FacultyModelAdmin(admin.ModelAdmin):
    exclude = ('slug',)
    list_display = ('id', 'name', 'dean')
    list_display_links = ('id', 'name')
    inlines = (DepartmentInline, UnitInline)


admin.site.register(Faculty, FacultyModelAdmin)  
admin.site.register(RequiredExamsList, RequiredExamsListModelAdmin)    