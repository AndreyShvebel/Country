from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework import permissions

from .models import Faculty, Department, Unit
from .serializers import FacultySerializer, DepartmentSerializer, UnitSerializer, FacultyDetailsSerializer

class FacultiesView(ListAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = FacultySerializer
    queryset = Faculty.objects.order_by('name')
        

class DepartmentView(ListAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = DepartmentSerializer
    #queryset = Department.objects.all()

    
    def get_queryset(self):
        queryset = Department.objects.all()
        faculty = Department.faculty
        queryset = queryset.filter(faculty__name=faculty)

        print(Department.objects.all())
        return queryset
    lookup_field = 'slug'

class UnitView(ListAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = UnitSerializer
    queryset = Unit.objects.all()
    lookup_field = 'slug'

class FacultyDetailsView(RetrieveAPIView):
    permission_classes = (permissions.AllowAny,)
    queryset = Faculty.objects.all()
    serializer_class = FacultyDetailsSerializer
    lookup_field = 'slug'
    
        
