from django.db import models
from django.template.defaultfilters import slugify
from phonenumber_field.modelfields import PhoneNumberField
from transliterate import translit, get_available_language_codes


class RequiredExamsList(models.Model):
    name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name


class Faculty(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    dean = models.CharField(max_length=50)
    faculty_phone = PhoneNumberField()
    faculty_office = models.PositiveIntegerField()
    faculty_email = models.EmailField()

    class Meta:
        verbose_name_plural = "faculties"
        index_together = (('id', 'slug'),)

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        initial_slug = slugify(self.name)
        slug = initial_slug

        count = 1
        while Faculty.objects.filter(slug=slug).exists():
            slug = f'{initial_slug}-{count}'
            count += 1

        self.slug = slug
        return super(Faculty, self).save(*args, **kwargs)


class Department(models.Model):
    name = models.CharField(max_length=100)
    department_phone = PhoneNumberField()
    department_office = models.PositiveIntegerField()
    department_email = models.EmailField()
    faculty = models.ForeignKey(Faculty, related_name='departments', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Unit(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(default='')
    budget_quantity = models.PositiveIntegerField(default=0)
    passing_score = models.PositiveIntegerField(default=0)
    required_exam1 = models.ForeignKey(RequiredExamsList, related_name='exam1', on_delete=models.CASCADE, default=1)
    required_exam2 = models.ForeignKey(RequiredExamsList, related_name='exam2', on_delete=models.CASCADE, default=1)
    required_exam3 = models.ForeignKey(RequiredExamsList, related_name='exam3', on_delete=models.CASCADE    , default=1)
    faculty = models.ForeignKey(Faculty, related_name='units', on_delete=models.CASCADE , default=1)

    def __str__(self):
        return self.name