const dataAnswer = [

    {
        id: 1,
        answer: 'Русский язык - 36 \n Профильная математика - 27\n История - 32 \n Обществознание - 42'
    },
    {
        id: 2,
        answer: `Вот ссылка с подробным описанием целевого обучения\n https://sibadi.org/abitur2021/lp/tselevoeobuchenie/`
    },
    {
        id: 3,
        answer: 'Вот ссылка с подробным описанием бюджетных мест \n https://sibadi.org/entrant/commission/priemnaya-komissiya-2021/'
    },
    {
        id: 4,
        answer: 'Да, вы можете оплатить обучение из средств материнского капитала. Для этого вам необходимо предоставить справку из ' +
            'Пенсионного фонда, однако, процесс оформления справки длится около 2-х месяцев, к тому же средств материнского капитала может не ' +
            'хватить на весь курс обучения (подробная информация о стоимости обучения). Поэтому мы рекомендуем вам запустить процедуру оформления ' +
            'справки и оплатить 1 семестр из собственных средств, а затем, начиная со 2 семестра, оплачивать обучение с помощью материнского капитала.'
    },
    {
        id: 5,
        answer: 'Минимальный бал для поступления в СибАДИ 192 балла'
    },
    {
        id: 6,
        answer: 'Приёмная комиссия находится по адресу Проспект мира 5, корпус 1, кабинет 139 \n' +
            'Часы работы приёмной комиссии с 09:00 - 17:00'

    },
    {
        id: 7,
        answer: 'Для получения подробной информации нажмите на кнопку `Боитесь забыть о поступлении` и вся ' +
            'необходимая информация поступит вам на почту'

    }
]

export default dataAnswer;