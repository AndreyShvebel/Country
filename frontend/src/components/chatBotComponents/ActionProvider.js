import dataAnswer from "./dataAnswer";

class ActionProvider {
    constructor(createChatBotMessage, setStateFunc, createClientMessage) {
        this.createChatBotMessage = createChatBotMessage;
        this.setState = setStateFunc;
        this.createClientMessage = createClientMessage;
    }

    greet = () => {
        const message = this.createChatBotMessage('Привет!')
        this.addMessageToState(message)
    }
    showScores = () => {
        const message = this.createChatBotMessage(`${dataAnswer[0].answer}`) //Минимальные баллы
        this.addMessageToState(message)
    }
    showTargetedTraining = () => {
        const message = this.createChatBotMessage(`${dataAnswer[1].answer}`); // Целевые места
        this.addMessageToState(message)
    }
    showBudgetPlaces = () => {
        const message = this.createChatBotMessage(`${dataAnswer[2].answer}`); // Бюджетные места
        this.addMessageToState(message)
    }
    showMaternalCapital = () => {
        const message = this.createChatBotMessage(`${dataAnswer[3].answer}`); // Материнский капитал
        this.addMessageToState(message)
    }
    showMinimumScore = () => {
        const message = this.createChatBotMessage(`${dataAnswer[4].answer}`); // Минимальный бал для поступления
        this.addMessageToState(message)
    }
    showCommission = () => {
        const message = this.createChatBotMessage(`${dataAnswer[5].answer}`); // Минимальный бал для поступления
        this.addMessageToState(message)
    }
    showInfo = () => {
        const message = this.createChatBotMessage(`${dataAnswer[6].answer}`); // Минимальный бал для поступления
        this.addMessageToState(message)
    }



    addMessageToState = (message) => {
        this.setState((prevState) => ({
            ...prevState,
            messages: [...prevState.messages, message]
        }))
    }
}

export default ActionProvider;