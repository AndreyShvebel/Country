import { createChatBotMessage } from "react-chatbot-kit";

const botName = 'RememBot'

const config = {
    botName: botName,
    initialMessages: [createChatBotMessage(`Привет, я ${botName}`)],

}

export default config