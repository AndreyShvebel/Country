import React from 'react';
import Chatbot from 'react-chatbot-kit'
import config from "./config";
import MessageParser from "./MessageParser";
import ActionProvider from "./ActionProvider";
import './chatBotComponent.css'

const ChatbotComponent = () => {
    return (

            <div className='chatBotContainer'>
                <Chatbot
                    config={config}
                    actionProvider={ActionProvider}
                    messageParser={MessageParser}
                />
            </div>

    );
};

export default ChatbotComponent;