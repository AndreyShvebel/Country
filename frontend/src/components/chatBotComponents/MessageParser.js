class MessageParser {
    constructor(actionProvider, state) {
        this.actionProvider = actionProvider;
        this.state = state;
    }

    parse(message) {
        console.log(message)
        const lowercase = message.toLowerCase()

        if(lowercase.includes('привет')){
            this.actionProvider.greet();
        }
        if(lowercase.includes('бал')){
            this.actionProvider.showScores();
        }
        if(lowercase.includes('целев')){
            this.actionProvider.showTargetedTraining();
        }
        if(lowercase.includes('бюджет')){
            this.actionProvider.showBudgetPlaces();
        }
        if(lowercase.includes('материнский')){
            this.actionProvider.showMaternalCapital();
        }
        if(lowercase.includes('минимал')){
            this.actionProvider.showMinimumScore();
        }
        if(lowercase.includes('приемная') || lowercase.includes('комис') || lowercase.includes('приёмная')){
            this.actionProvider.showCommission();
        }

        if(lowercase.includes('поступление') || lowercase.includes('документ') || lowercase.includes('срок')){
            this.actionProvider.showInfo();
        }

    }
}

export default MessageParser;