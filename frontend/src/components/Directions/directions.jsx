import React from 'react';
import {Link} from 'react-router-dom'
import axios from 'axios';
import { useEffect, useState } from 'react';

import './directions.css'
import {Navigation} from 'react-minimal-side-navigation';
import 'react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css';


    const Directions = () => {

        // const [faculty, setFaculty] = useState({});
        // const [loading, setLoading] = useState(true);

        // useEffect(() => {
        //     fetch(`${process.env.REACT_APP_FACULTIES_ENDPOINT}`)
        //     .then(response => response.json())
        //     .then(data => console.log(data));

        
        // }, []);

        return (
            
                
            <div className="nav_facs">

                <div className='facs_list'>
                    <h1>Факультеты:</h1>
                    <Link className='fac' to='/Isu'>ИСУ</Link>
                    <Link className='fac' to='/adm'>АДМ</Link>
                    <Link className='fac' to='/Isu'>АТ</Link>
                    <Link className='fac' to='/adm'>ЗФ</Link>
                    <Link className='fac' to='/Isu'>НСТ</Link>
                    <Link className='fac' to='/adm'>ПГС</Link>
                    <Link className='fac' to='/Isu'>ИМА</Link>
                    <Link className='fac' to='/adm'>ЭиУ</Link>
                </div>
            </div>
            
            
                



                // <div className='nav-bar-direction'>
                //     <Navigation
                //         // you can use your own router's api to get pathname
                //         activeItemId="/management/members"
                //         onSelect={({itemId}) => {
                //             // maybe push to the route
                //         }}
                //         items={[
                //             {
                //                 title: 'Направление подготовки',
                //                 itemId: '/management',
                //                 elemBefore: () => <Icon name="users"/>,
                //                 subNav: [
                //                     {
                //                         title: 'Факультет ИСУ',
                //                         itemId: '/management/isu',
                //                     },
                //                     {
                //                         title: 'Факультет АДМ',
                //                         itemId: '/management/adm',
                //                     },
                //                     {
                //                         title: 'Факультет АТ',
                //                         itemId: '/management/at',
                //                     },
                //                     {
                //                         title: 'Факультет ЗФ',
                //                         itemId: '/management/zf',
                //                     },
                //                     {
                //                         title: 'Факультет ИМА',
                //                         itemId: '/management/ima',
                //                     },
                //                     {
                //                         title: 'Факультет НСТ',
                //                         itemId: '/management/nst',
                //                     },
                //                     {
                //                         title: 'Факультет ПГС',
                //                         itemId: '/management/pgs',
                //                     },
                //                     {
                //                         title: 'Факультет ЭиУ',
                //                         itemId: '/management/aiy',
                //                     },
                //                 ],
                //             },

                //         ]}
                //     />
                // </div>
        );
    };



export default Directions;