import React from 'react';
import './admission.css'
import {Navigation} from "react-minimal-side-navigation";
//import Icon from "awesome-react-icons";

const Admission = () => {
    return (
        <div className="wraper">
            <div className='admission'>
            <h1>Для того, чтобы поступить в СибАДИ</h1>необходимо хорошо подготовиться к сдачи ЕГЭ. Для этого в академии работают подготовительные курсы,
             которые готовят абитуриентов по предметам вступительных испытаний: математике, физике, русскому языку, обществознанию и рисунку.
        </div>
        </div>
        
        // <div className='nav-bar-direction'>
        //     <Navigation
        //         // you can use your own router's api to get pathname
        //         activeItemId="/management/members"
        //         onSelect={({itemId}) => {
        //             // maybe push to the route
        //         }}
        //         items={[
        //             {
        //                 title: 'Как поступить',
        //                 itemId: '/howStudy',
        //                 elemBefore: () => <Icon name="users"/>,
        //                 subNav: [
        //                     {
        //                         title: 'Школьникам',
        //                         itemId: '/management/school',
        //                     },
        //                     {
        //                         title: 'Студентам колледжей',
        //                         itemId: '/management/kolledj',
        //                     },
        //                     {
        //                         title: 'Магистрантам',
        //                         itemId: '/management/magistr',
        //                     },
        //                     {
        //                         title: 'Аспирантам',
        //                         itemId: '/management/aspirant',
        //                     }
        //                 ],
        //             },

        //         ]}
        //     />
        // </div>
    );
};

export default Admission;