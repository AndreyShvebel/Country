export const MenuItems = [
    {
        title: 'Как поступить',
        url: 'Admission',
        cName: 'nav-links'
    },
    {
        title: 'Направления подготовки',
        url: 'Directions',
        cName: 'nav-links'
    },
    {
        title: 'Подача документов',
        url: 'Documents',
        cName: 'nav-links'
    },
    {
        title: 'Боитесь забыть о поступлении?',
        url: '#',
        cName: 'nav-links-mobile'
    },

]