import React, { useState } from 'react';
import { BrowserRouter } from "react-router-dom";
import './App.css';
import Navbar from "./components/Navbar/Navbar";
import Directions from "./components/Directions/directions";
import Route from "react-router-dom/es/Route";
import ChatbotComponent from "./components/chatBotComponents/chatBotComponent";
import Admission from "./components/Admission/admission";
import Documents from "./components/Documents/documents";
import Isu from './components/facs/Isu' 
import Adm from './components/facs/Adm'


const App = (props) => {

    const [visibility, setVisibility] = useState(false);

    return (


        <BrowserRouter>
            <div className='App'>
                <section>
                    <Navbar />

                    {visibility ? <ChatbotComponent /> : null}

                    <div className='app-wrapper-content'>
                        <Route path='/admission' component={Admission} />
                        <Route path='/directions' component={Directions} />
                        <Route path='/documents' component={Documents} />
                        <Route path='/isu' component={Isu} />
                        <Route path='/adm' component={Adm} />

                    </div>
                    <div className='chat-pic'>
                        {visibility ?
                            null
                            :
                            <img src={require('./Picture/chatPic.svg').default} onClick={() => setVisibility(!visibility)} />
                        }
                    </div>
                    <div className="chat-close">
                        {visibility ?
                            <img src={require('./Picture/chatClose.svg').default}
                                onClick={() => setVisibility(!visibility)} />
                            :
                            null
                        }
                    </div>
                    <div className="wave wave1"></div>
                    <div className="wave wave2"></div>
                    <div className="wave wave3"></div>
                    <div className="wave wave4"></div>
                </section>
            </div>

        </BrowserRouter>
    );
}

export default App;
